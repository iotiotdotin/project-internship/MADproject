**Introduction:**  
----------------------------------------  
![vs](https://lh3.googleusercontent.com/RuGVwImVpWEy2KJppNdqg06vsN94RkHm60JqclZEr45K3AORQvKzSYwcgY7c2jX-2Xci5w=s3700)  

* A vibration which propagates as a pressure wave in the air or any other elastic medium is called _sound_.   
* Humans can hear sound in the frequency range of 20 Hz-20 kHz, whereas that above 20 kHz is _ultrasound_ and that below 20 Hz is called _infrasound_.  
* To deal with the science of sound, the term *_acoustics_*, was introduced.  
* The instrument used to convert these sound pressure waves into electrical signals is known as a _microphone_.  
* The conversion of sound pressure waves into electrical signals is performed by different transduction mechanisms, i.e., piezoelectric,piezoresistive,optical, and capacitive.  

**_Piezoelectric Microphone:_**  
---------------------------------
![pm](https://electronics-club.com/wp-content/uploads/2019/11/Crystal-Microphone.png)    

* This type of microphone works on the principle of _piezoelectric effect_. The piezoelectric effect can be direct or inverse.  
* In the case of a microphone, direct piezoelectric effect happens, in which stress is applied in terms of sound waves, which then results in creating charges on the plates. This is further converted to voltage as an output signal.  
The commonly used piezoelectric materials are _zinc oxide (ZnO)_,_aluminum nitride (AlN)_, and _lead zirconium titanate (PZT)_.  
* Both ZnO and AlN are nonferroelectric materials while PZT is a ferroelectric material, i.e., its direction of polarization can be reversed by applying an electric field.  

**Piezoresistive Microphone:** 
--------------------------------------------------------------- 

![pr](https://lh3.googleusercontent.com/zcYEthP1LoDChnJMcCywZvyxWHK3sN85bldLXQYAvdkkEeDK9ebwC6Z8mycsVJdkNiZJ=s400)  

* A microphone in which change in the electrical resistance occurs due to sound waves of the semiconductor material is considered to be a _piezoresistive microphone_.  
* A piezoresistive microphone is composed of a diaphragm with two pairs of piezoresistors in a Wheatstone bridge configuration.  
* Due to the mechanical stress or strain, a change in resistivity of a piezoresistive material occurs.  

**Optical Microphone:**  
------------------------------------------------------------------------
![om](https://lh3.googleusercontent.com/ubvISxhU04Fn9uwTRqzjNlwMKWGB9f2PvRZSSHMXqZdUDh25gGlAUrKERhH8Mep8OJKnpg=s550)  

* In the case of an optical microphone, transduction mechanism of converting the sound waves into electrical signals is produced by sensing changes in light intensity.  
* The modulated light has three properties, i.e., intensity, polarization, and phase.  
* The simplest intensity-modulated microphone can be constructed with an LED.  
* Optical microphones are used in applications where the canceling of noise is required, ex. in directional microphones. 

**Capacitive Microphone:**  
---------------------------------------

* In capacitive microphones, a change in capacitance occurs between the static back plate and moving diaphragm which then converts into electrical signals through electronic circuitry.  
* Because of their high sensitivity, flat frequency response, and low noise levels, capacitive microphones are the focus of researchers.  
* Capacitive MEMS microphones are motion sensors composed of two parallel plates separated by an air gap and work on the principle of a mass-spring system where the moving membrane is acting as a spring, , in which _“V”_ represents the supplying voltage, _“x”_ represents the displacement of the membrane, and _“Co”_ represents the nominal capacitance between the back plate (fixed plate) and the membrane. _“K”_ is the mechanical stiffness of the spring.  
* The mechanical force _"Fm"_  of this mass-spring system is given by the following equation:  
>_Fm=Kx_ (Spring Equation)    

They are futher subdivided into following 2 types:  

1. **_Simple Condenser Microphone:_** 
---------------------------------------------- 
![sm](https://www.electronics-notes.com/images/microphone-condenser-capacitor-construction-01.svg)  

* A condenser is another term for a capacitor. A capacitive microphone that is polarized externally is simply known as a condenser microphone.  

2. **_Electret Condenser Microphone:_** 
--------------------------------------------- 
![cm](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfznpQJTgW92-DiHnrui9t3oiHlAtH2wgkwA&usqp=CAU)  

* The microphone that is polarized internally using an electret is called an electret microphone. The term electret is a dielectric material that has to be polarized permanently to create charges on the capacitive plates.  
* Electret microphones can also be sensitive to acceleration.  
* Traditional ECMs consisted of air gap capacitors with a moving diaphragm and a back plate.  
* Their stability, repeatability, and performance were not good over various temperatures and other environmental conditions.  
* MEMS technology has revolutionized this industry by developing ultra-small-size, lightweight, low-power, low-cost, and compact-size devices.  
* The performance of MEMS sensors is superior, have less variation in sensitivity over temperatures, and have lower vibration sensitivity than traditional ECMs.  
* MEMS microphones can be easily integrated with other microlevel circuitry, which makes it perfect for various applications.  
* MEMS microphone contains a back plate and a flexible membrane fabricated on a silicon wafer. The perforated back plate allows the sound pressure wave to enter, which causes the flexible membrane to move. The movement of the flexible membrane causes a capacitance variation between the membrane and a fixed plate, which is converted into an electrical signal through various types of interface circuitry. 

**Directional Microphones:**  
---------------------------------
![dm](https://lh3.googleusercontent.com/AXY7daGCWYa_ymtobIVeHQHedWkAjYN24lE65s5p_52ncq6Bpp4BwSJye_hlPtCz-z4mpQ=s450)  

* In applications where the background noise cancellation is the greatest consideration, directional microphones are the first choice for researchers and developers.

* In terms of directionality, MEMS microphones can be divided into two types:  

**_(1) Omnidirectional microphone:_**  
------------------------------   
* A microphone which receives sound waves from all directions equally, i.e., it shows the same sensitivity to the sound source at every different position.  
* This type of microphone is a good candidate for an application where the sound source changes its position around the microphone.  

**_(2) Directional microphone._**  
-----------------------------------------
* Instead of generating an electrical response from sound waves arriving from all directions around the device, the directional microphone has its strongest output when sound waves arrive along a single axis vertical through or parallel with the surface of a moving membrane.


| Polar Pattern  |  Omnidirectional | Directional  |  
|----------------|-------------------|-------------|
| Gain to feedback ratio  | Lower  |  Higher |   
|  Feedback build-up | Slow  | Fast  |
| Off-axis colouration  | Smooth and even  | Typically less smooth |
| Proximity effect  | No  | Yes  |  
| Sensitivity to wind, handling and popnoises  | Low  | High  |  
| Distortion  | Lower | Higher   |  
| Channel seperation  |  Near field: Good Diffuse field: Less precise |  Near field: Good Diffuse field: Good  |        

**Available Arduino Microphone Module In Market:**  
-------------------------------------------------------------

| Part Image  |  Part number | Name  |  Manufacturer  |  Link to data sheet  |  Frequency response  |  Operating voltage  |  Features  |  
|-------|---------|-------|----------|-----------|-------|---------|----------|  
|  ![gh](https://lh3.googleusercontent.com/rtciw-RUoM_8hMhOPZKk6YWeQX9qWnqOK4yD1Z7aXutaDaLXVIyl09itwwQ3H4SxVkCb=s450)  |  MAX4466  |  Electret Microphone Amplifier with adjustable gain  |  Maxim Integrated Products  |    [datasheet1](https://html.alldatasheet.com/html-pdf/460080/MAXIM/MAX4466/150/1/MAX4466.html)  |  200-600 KHz  |  2.4-5.5 V  |  The Amplifier has excellent power supply noise rejection.  |  
|  ![m4](https://lh3.googleusercontent.com/3dYvM80wAJWLYJRgY9egIPgqfePRsUiefTjLQvl2dGGCnNOyWXcmzmJsDVIJV5ghpIDm=s400)  |  MAX4063  |  Differential Microphone Preamplifier with Internal Bias and Complete Shutdown  |  Maxim Integrated Products  |  [datasheet2](https://datasheets.maximintegrated.com/en/ds/MAX4063.pdf)  |  1kHz  |  2.4-5.5 V  |   Differential-input microphone preamplifier optimized for high-performance, portable applications  |  
|  ![m0](https://lh3.googleusercontent.com/H9LGeD2PxFohEVSdm92Osab91M4KleVGFeQzgrfmopnVDrF1EkF-jeZGZMlkihnBrrYz=s400)  |  MAX4060  |  	Differential Microphone Preamplifiers With Internal Bias And Complete Shutdown  |  Maxim Integrated Products  |  [datasheet3](https://pdf1.alldatasheet.com/datasheet-pdf/view/171571/MAXIM/MAX4060.html)  |  192 Khz  |  2.4-5.5 V  |  These devices feature adjustable gain with excellent power-supply rejection and common-mode rejection ratios, making them ideal for low-noise applications in portable audio systems.  |
|  ![hg](https://lh3.googleusercontent.com/QPNOSakyMrhqKaKx5bIbb1FWZhRSY6BHGw8-84o_a0nC-KF5C_MrWoVslFOvtG6zNUCoqKg=s500)  |  KY038   |  Microphone sound sensor module  |   Keyes  | [datasheet4](https://datasheetspdf.com/datasheet/KY-038.html)   | 20-50 KHz   | 3-24 V   | Single channel signal output.  |  
|  ![fh](https://lh3.googleusercontent.com/wCQgfImCnjUo3NEBnd-nQgtCxSZX2HyHBYxJ1Jhk8XUUp5EZH4nMIEZI2ac6ihn-Ar62gw=s500)  |  SPH0645  |  Adafruit I2S MEMS Microphone  |  Knowles  | [datasheet5](https://cdn-shop.adafruit.com/product-files/3421/i2S+Datasheet.PDF)   |  32-64 KHz  | 600µA  |  Suitable for almost all common audio recording/detection.  | 
|  ![sp](https://lh3.googleusercontent.com/neQExFkVS1CjI51eE1_ZIp0-Yvqs9EJsIxga_32C8VqhLwxIKq_xlccJXlA-VgI1Tcu3=s450)  |  SPH0611LR5H-1  |  MICROPHONE MEMS ANALOG OMNI  |  Knowles  |  [datasheet6](https://www.alldatasheet.com/datasheet-pdf/pdf/546562/KNOWLES/SPH0611LR5H-1.html)  |  100Hz-10kHz  |  1.5-3.6 V  |  Miniature, high-performance, low power, bottom portsilicon microphone.  | 
| ![45](https://lh3.googleusercontent.com/I8BsvcQiy0kKlirSzGx2xki7Jq9_757cF1HUsJT8KtzwNfmap-fBkRX8AV726hTAPReqNfw=s450)  |  SPH0644LM4H-1  |  CORNELL MIC MULTIMODE DGT BOT PT  |  Knowles  |  [datasheet7](https://www.alldatasheet.com/datasheet-pdf/pdf/1246101/KNOWLES/SPH0644LM4H-1.html)  |    20Hz-10kHz  |  1.6V-3.6V  |  Miniature, high-performance, low power, bottom portsilicon digital microphone with a single bit PDM output,using knowledge proven high performance Si sonic MEMS technology.  |










