## Setup of Docker with Grafana and Influxdb

#### Docker:
Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.

![Docker](https://devopedia.org/images/article/101/8323.1565281088.png)

Installation:
<br>Pull docker and then install using the below command:-
<pre><code>$ curl -sSl https://get.docker.com | sh</code></pre>

<br>Create a docker network to allow all the images to communicate with each other.
<pre><code>$ sudo docker network create -d bridge network</code></pre>
- "network"=name of our bridged network

<br>Add user using the below command:-
<pre><code>$ sudo usermode -aG docker $USER</code></pre>
- $USER=name of the user

#### Grafana in Docker

<b>Installing Grafana in Docker</b>
<br>The Grafana docker image is stored under the Grafana [repository](https://hub.docker.com/r/grafana/grafana).

Run the latest stable version of grafana using the below command on the host:-
<pre><code>$ docker run -d -p 3000:3000 grafana/grafana</code></pre>

<br>Create a docker volume with the name grafana-storage:
<pre><code>$ docker volume create grafana-storage</code></pre>

<br>Run the below command so that we can run the container:-
<pre><code>$ sudo docker run -d -p 3000:3000 --network="network" --name=grafana-storage:/var/lib/grafana grafana/grafana</code></pre>
- "network"=name of our network

<br>Run the below command to see our container:-
<pre><code>$ docker container ls</code></pre>

<br>Now with a web browser, head over to http://localhost:3000
<br>It should be redirected to Grafana homepage. The default credentials for Grafana are admin/admin. Immediately, you are asked to change your password. Choose a strong password and click on “Save.”

![Homepage](https://cdn.thenewstack.io/media/2019/11/7ba6f94e-influxdata7.png)

<br>Now it should be redirected to the Grafana default Web UI:-

![Web UI](https://cdn.thenewstack.io/media/2019/11/0e2ce688-influxdata7.png)

#### Influxdb in Docker:

The official InfluxDB image for Docker is called [influxdb](https://hub.docker.com/_/influxdb).

<b>Running the influxdb container</b>
<br>The InfluxDB image exposes a shared volume under /var/lib/influxdb, so we can mount a host directory to that point to access persisted container data.
<pre><code>$ docker run -p 8086:8086 -v $PWD:/var/lib/influxdb influxdb</code></pre>
- Modify $PWD to the directory where we want to store data associated with the InfluxDB container.

<br>Run the below command to mount a configuration file and use it with the server. It will generate a default configuration file:-
<pre><code>$ docker run --rm influxdb influxd config > influxdb.conf</code></pre>

<br>Keep all the default settings of the config file.
 
To start InfluxDB on Docker, run the following command:-
<pre><code>$ docker run -d -p 8086:8086 --name=influxdb --network="network" -v $PWD:/etc/influxdb/influxdb.conf:ro influxdb --config /etc/influxdb/influxdb.conf</pre></config>
- Modify $PWD with the directory where the config file of the influxdb exists.
- network = name of our network

<b>Testing the InfluxDB Container</b>
<br>In order to test if the InfluxDB container is correctly running, we can check that the HTTP API is correctly enabled:-
<pre><code>$ curl -G http://localhost:8086/query --data-urlencode "q=SHOW DATABASES" </pre></code>

<pre><code>$ docker container ls </code></pre>
![](https://cdn.thenewstack.io/media/2019/10/cf43331b-influxdb11.png)

<br>Now head over to the grafana UI:

Click on “Add data source” to add an InfluxDB datasource:

![](https://cdn.thenewstack.io/media/2019/11/a7b1f37a-influxdata9.png)

Next, select the InfluxDB option and click on “Select.”

![](https://miro.medium.com/max/700/0*FECckMUhT-v_vaXH)

There select Type = InfluxDB, give the Name for this data source, then put the URL using our influxdb container name as address.

<br>Then insert the Database name and user/password for our database, these parameters was created by previously running the Influxdb container.
Click on Save & Test to see that your data source is OK:

![](https://miro.medium.com/max/700/0*t02qDBYSGrqP2dew)
