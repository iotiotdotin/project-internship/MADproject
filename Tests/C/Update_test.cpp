/*

## Goal:   Code TO CONVERT THE GIVEN STRING TO JS FORMAT 
        @Brief : 
        configurations for the Raspberry pi is set with the help 
         of a web UI which sends out the data in this form

        @author R Animesh 

The code is implemented using string and substring parameter link for further reference.  
https://www.geeksforgeeks.org/substring-in-cpp/#:~:text=In%20C%2B%2B%2C%20std%3A%3A,sub%2Dstring%20of%20this%20object.


The substring is used to store all the data collected from the string and break them into nesscary parts .

The function read  and write the values and write in config.js file  in the specified format. 
string type can be changed according to the user input. 

example : input index used
                 0  1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16


                 w  i  f  i  S  S  I  D  =  s   h   u   n   y   a   A   P
                 
general formula :
that I is why printing  useful details that are after the length() of string.  in this case "wifiSSID" has length equal to 8 so printing the elements after it. hence it starts from 9


## Case 1
input string 

 wifiSSID=shunyaAP&wifiPSK=password&mqttBrokerUrl=192.168.0.2:1883&mqttUsername=raspberry&mqttClientId=rpi-800&mqttPassword=shunya&influxdbUrl=148.251.91.243:8086&influxdbDBName=shunya_test

## Case 2
input string 

wifiSSID=shunyaAP&wifiPSK=&mqttBrokerUrl=192.168.0.2:1883&mqttUsername=&mqttClientId=rpi-800&mqttPassword=&influxdbUrl=148.251.91.243:8086&influxdbDBName=shunya_test


## OS
Windows 10 / Ubuntu 16.0 


COMPILING CODE

Create new document with extension .cpp in your text-editor

Code in that document and save it.
Open command line.
 for compiling in local os we need the following commands 
  1.  g++ string.cpp 
  2. ./a.exe 
 
 the output will be shown . 
 
## Output :
The output consists of the variables that is present in the print function.
For this code, it gives 6 values as output.


 {
        "Wifi": {
            "SSID": "shunyaAP",
            "PSK" : "password"
        },
        "mqtt": {
            "brokerUrl": "192.168.0.2:1883",
            "username": "raspberry",
            "password": "shunya",
            "clientId": "rpi-800"
        },
        "influxdb": {
            "influxdbUrl": "148.251.91.243:8086",
            "influxdbDBName": "shunya_test"
        }
}

## Errors faced:

 File not found error.


 */



/*########################################################################################*/

/** ---- Includes ---- */
/* ------------------- */

/** Standard includes*/


#include <fstream>
#include <iostream>
#include <bits/stdc++.h>  // includes all header files
  

// Shunya interfaces header file
//#include <si/shunyaInterfaces.h>

// JSON parsing library 
//#include "json/json.h" // to extract the data from JSON file in respective data type



using namespace std;


#define ssid_index 9
#define psk_index  8
#define brokenurl_index  14
#define username_index 13
#define password_index 13 
#define clentid_index  13
#define influxdburl_index 12
#define influxdbName_index 15


 void  wifiSSID(string f,ofstream & outfile){

  outfile<<"{\n";                      // opening brackets 
   
           outfile<<"\t\"Wifi\" : {"<<endl; 
          outfile<<"\t\t"<<"\"SSID\" : "<<"\"";    
            for(int i=ssid_index;i<f.length();i++)
            outfile<<f[i];

             outfile<<"\""<<endl;   

return ;
 }

 
 void  PSK(string f,ofstream & outfile){

   outfile << "\t\t"<<"\"PSK\"  : "<<"\""; 
           
            for(int i=psk_index;i<f.length();i++) 
              outfile<<f[i];
        
           outfile<<"\""<<endl;  
           // cnt++;
           
            outfile<<"\t },\n";             // closing bracket for specific item

   return ;
 }


 void brokenurl(string f,ofstream & outfile){

   outfile<<"\t\"mqtt\" : {"<<endl;        
              
              outfile<<"\t\t"<<"\"brokenUrl\": "<<"\"";        
                for(int i=brokenurl_index ;i<f.length();i++) 
                 outfile<< f[i];
        
                outfile<<"\", "<<endl; 
          // cnt++;

       return ;
    }   

  void  username(string f,ofstream & outfile){   
           if(f.size() == 13)
          outfile<<"\t\t"<<"\"username\": \"\","<<endl;      // Corner case for username
         else {
          outfile<<"\t\t"<<"\"username\": "<<"\"";     
        
         for(int i=username_index ;i<f.length();i++) 
                   outfile<<f[i];
            
             outfile <<"\", "<<endl;  }

  return ;         
  }

           
  void password(string f,ofstream &outfile){

         outfile<<"\t\t"<<"\"password\": "<<"\""; 
        for(int i=password_index ;i<f.length();i++)   
          outfile<<f[i];
             
            outfile<<"\", "<<endl;   
 
  return ;
 }          


 void influxdbUrl(string f, ofstream &outfile){
            outfile<<"\t\"influxdb\" : {"<<endl;         // another  bracket for specific item 
          

              outfile<<"\t\t"<<"\"influxdbUrl\": "<<"\"" ; 
                   for(int i=influxdburl_index ;i<f.length();i++)   outfile<<f[i];
             
              outfile<<"\", "<<endl;     

            return ;
    }  

 void influxdbName(string f,ofstream &outfile){

             outfile<<"\t\t"<<"\"influxdbDBName\": "<<"\"";  

               for(int i=influxdbName_index;i<f.length();i++) 
                      outfile <<f[i];

               outfile<<"\""<<endl; 
              
              outfile<<"\t }\n";            // closing bracket for specific item      
         
          
              outfile <<"}\n";  

      return ;
      }       

 
 void client_id(string store,ofstream & outfile){

      outfile<<"\t\t"<<"\"clientId\": "<<"\"";
        for(int i=13;i<store.length();i++)    outfile<<store[i];
             
             outfile<<"\""<<endl;
            outfile<<"\t },\n";               // closing bracket for specific item
                
               
            
    return ;
     }  

  void solve(){

   ofstream outfile;        // open a config.js file in write mode.
   outfile.open("config.js");    

      vector<string>v(8);
        v.clear();
  
    
 //  Case for 1st input
   //  char s[189] = "wifiSSID=shunyaAP&wifiPSK=password&mqttBrokerUrl=192.168.0.2:1883&mqttUsername=raspberry&mqttClientId=rpi-800&mqttPassword=shunya&influxdbUrl=148.251.91.243:8086&influxdbDBName=shunya_test";
    // Case for 2nd input
  //  char fi[] = "wifiSSID=shunyaAP&wifiPSK=&mqttBrokerUrl=192.168.0.2:1883&mqttUsername=&mqttClientId=rpi-800&mqttPassword=&influxdbUrl=148.251.91.243:8086&influxdbDBName=shunya_test";

   char s[189];     // declaring the character array 
   cin>>s;       // dynamic input 
    

  char* token = strtok(s,"&");  // return first token 
 

  while(token != NULL){  
                            // keep printing tokens while one of the 
                             // delimiters present in str[]

         v.push_back(token);
      token = strtok(NULL, "&");
  }



    int flag = 0;
    string store;

      for(auto it:v)
      {
          string f = it;
   
          if(f.length() > 0)
          {

          //  cout<<f<<"                      "<<f.substr(0,14)<<endl;
            
           if(f.substr(0,8) == "wifiSSID")        // wifiSSID  
              wifiSSID(f,outfile);

          else if(f.substr(0,7) == "wifiPSK"){   // wifiPsk 
       
              
          if(f.length() == 8)                         // Corner Case for PSk
             outfile<< "\t\t"<<"\"PSK\"  : "<<"\""<<"\""<<endl;    
           else
               PSK(f,outfile);
                   
           }
           else if(f.substr(0,13) == "mqttBrokerUrl")   // mqttBrokerUrl  
                   brokenurl(f,outfile);

           else  if(f.substr(0,12) == "mqttUsername")   // mqttusername   
              username(f,outfile);
           
           else if(f.substr(0,12) == "mqttClientId")   // mqttClientId
          {
           flag = 1;   
           store = f;
           continue;  
           }
           else if(f.substr(0,12) == "mqttPassword"){      // mqtt password
             
             if(f.length() == 13)
                 outfile<<"\t\t"<<"\"password\": "<<"\""<<"\","<<endl;    // ## Corner Case for password
           else 
               password(f,outfile);
          }
          else if(f.substr(0,11) == "influxdbUrl")          //  influxdbUrl  
                 influxdbUrl(f,outfile);

           else if(f.substr(0,14) == "influxdbDBName")   // influxdbDBName
              influxdbName(f,outfile);
     
        
          if(flag == 1) {
             client_id(store,outfile);      
                flag = 0; 
            }
                
         }
      
       }
  
   outfile.close();       // closing the file after writing in it
      
     return ; 
  }


int main (){
 
  
    solve();  // calling the functions writing in the file

  return 0 ;
}

